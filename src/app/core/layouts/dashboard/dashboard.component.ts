import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/api';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  items!: MenuItem[];
  home!: MenuItem;

  ngOnInit() {
    this.items = [
      { label: 'Mis Imágenes' }
    ];
    this.home = { icon: 'pi pi-home' };
  }
}
