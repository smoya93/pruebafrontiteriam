import { Injectable } from '@angular/core';
import { loremIpsum } from 'lorem-ipsum';
import { Observable, of } from 'rxjs';
import { ImageInterface } from '../models/image.interface';

@Injectable({
  providedIn: 'root'
})
export class DataImageService {

  constructor() { }

  public getDataListImagen(limitRamdom: number): Observable<ImageInterface[]> {
    return of(this.getDataRamdom(limitRamdom));
  }

  public getDataRamdom(limitRandom: number): ImageInterface[] {
    const newItems: ImageInterface[] = [];
      for (let i = 0; i < limitRandom; i++) {
        const id = Math.round(Math.random() * 100);
        const photo = `https://picsum.photos/200?random=${id}`;
        const text = loremIpsum();
        newItems.push({
          id: id.toString(),
          photo: photo,
          text: text
        });
      }
      return newItems;
  }
}
