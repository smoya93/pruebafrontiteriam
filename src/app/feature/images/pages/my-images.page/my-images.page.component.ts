import { CdkVirtualScrollViewport } from '@angular/cdk/scrolling';
import { Component, OnInit, ChangeDetectionStrategy, ViewChild, AfterViewInit, NgZone, ChangeDetectorRef } from '@angular/core';
import { ImageInterface } from '../../models/image.interface';
import { firstValueFrom, timer } from 'rxjs';
import { DataView } from 'primeng/dataview';
import { LazyLoadEvent } from 'primeng/api';
import { Table } from 'primeng/table';
import { loremIpsum, LoremIpsum } from "lorem-ipsum";
import { DataImageService } from '../../services/data-image.service';

@Component({
  selector: 'app-my-images.page',
  templateUrl: './my-images.page.component.html',
  styleUrls: ['./my-images.page.component.scss']
})
export class MyImagesPageComponent implements OnInit {
  @ViewChild('dt1') dt1!: Table;
  myPageList: ImageInterface[] = [];
  limitRandom: number = 4000;
  loading: boolean = false;

  constructor(private dataImageService: DataImageService) { }

  ngOnInit() {
    this.loading = true;
    this.getDataImageList();
  }

  async getDataImageList() {
    try {
      let val = await firstValueFrom(this.dataImageService.getDataListImagen(this.limitRandom));
      this.myPageList = val;
      this.loading = false;
    } catch (error) {
      console.error(error);
      this.loading = false;
    }
  }

  applyFilterGlobal($event: Event, stringVal: string) {
    this.loading = true;
    this.dt1.filterGlobal(($event.target as HTMLInputElement).value, stringVal);
    setTimeout(() => {
      this.loading = false;
    }, 1000);
  }
}
