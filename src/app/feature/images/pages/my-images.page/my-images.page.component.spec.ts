import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MyImagesPageComponent } from './my-images.page.component';

describe('MyImagesPageComponent', () => {
  let component: MyImagesPageComponent;
  let fixture: ComponentFixture<MyImagesPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MyImagesPageComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(MyImagesPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
