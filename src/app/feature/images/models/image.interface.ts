export interface ImageInterface {
    id: string;
    photo: string;
    text: string;
}

