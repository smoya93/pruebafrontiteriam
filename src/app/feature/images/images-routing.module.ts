import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MyImagesPageComponent } from './pages/my-images.page/my-images.page.component';

const routes: Routes = [
  {
    path: '',
    component: MyImagesPageComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ImagesRoutingModule { }
