import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ImagesRoutingModule } from './images-routing.module';
import { MyImagesPageComponent } from './pages/my-images.page/my-images.page.component';
import { AppModule } from 'src/app/app.module';


import { MatToolbarModule } from '@angular/material/toolbar';
import { MatListModule } from '@angular/material/list';
import { MatDividerModule } from '@angular/material/divider';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { ScrollingModule } from '@angular/cdk/scrolling';
import {MatFormFieldModule} from '@angular/material/form-field';
import { FormsModule } from '@angular/forms';
import {DataViewModule} from 'primeng/dataview';
import {InputTextModule} from 'primeng/inputtext';
import {VirtualScrollerModule} from 'primeng/virtualscroller';
import {ToolbarModule} from 'primeng/toolbar';
import {ButtonModule} from 'primeng/button';
import {TableModule} from 'primeng/table';

@NgModule({
  declarations: [
    MyImagesPageComponent
  ],
  imports: [
    CommonModule,
    ImagesRoutingModule,
    MatListModule,
    MatToolbarModule,
    MatListModule,
    MatDividerModule,
    MatButtonModule,
    MatIconModule,
    MatMenuModule,
    MatProgressSpinnerModule,
    ScrollingModule,
    MatFormFieldModule,
    FormsModule,
    DataViewModule,
    InputTextModule,
    VirtualScrollerModule,
    ToolbarModule,
    ButtonModule,
    TableModule
  ]
})
export class ImagesModule { }
